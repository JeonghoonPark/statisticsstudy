#include "./config/compileConfig.hpp"

#include <iostream>

namespace hoonStat{
    
    /* Error Code */
    typedef enum
    {
        mean_OK = 0,
        mean_INPUT_SIZE_0,
    } RET_mean;

    /* Function Definition */
    template <typename T>
    RET_mean mean(std::vector<T> input, T output);
    
    /* Test */
    void Test_mean(void);
}