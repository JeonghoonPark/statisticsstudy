#include "3_mean.hpp"

template <typename T>
hoonStat::RET_mean hoonStat::mean(std::vector<T> input, T output)
{
    using namespace hoonStat;

    RET_mean ret = mean_OK;

    if (input.size == 0)
    {
        ERROR_LOG("input.size is 0");

        ret = mean_INPUT_SIZE_0;
        return ret;
    }

    T sum = 0;

    for (const auto& it : input)
    {
        sum += it;
    }

    output = sum / static_cast<T>(input.size());
}

void hoonStat::Test_mean(void)
{
    /* Test 1 */
    std::vector<double> Test1_input = {2.0, 3.0, 1.0};
    double Test1_output = 0.0;

    hoonStat::RET_mean Test1_ret = hoonStat::mean(Test1_input, Test1_output);
        //expect 2.0
    if (Test1_ret != hoonStat::mean_OK)
    {
        ERROR_MSG(Test1_ret);
        ERROR_LOG("error occured in Test1");
    }

    

    /* Test 2 */
}