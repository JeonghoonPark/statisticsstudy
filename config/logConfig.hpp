/* Message Definition */
#include <spdlog/spdlog.h>
#include <spdlog/logger.h>
#include <spdlog/sinks/sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <iostream>

#define _CONSOLE_LOG_LEVEL  (spdlog::level::trace)
#define _FILE_LOG_LEVEL     (spdlog::level::trace)

#define _LOG_PATH           ("logs/hoonStat.log")

#define FUNCTION_IN         (">>")
#define FUNCTION_OUT        ("<<")

#define UNIT_IN(_unitName)  (std::to_string("  >>")