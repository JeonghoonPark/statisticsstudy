# 1. Log Print

1. trace
   - method(or function) in&out with FUNCTION_IN and FUNCTION_OUT defined in logConfig.hpp
   - in a method, a unit in&out
2. info
   - in a method, if-statement or for-statement information
3. debug
   - calculation resultsd
4. warn
   - warn occured in any time
5. error
   - error occured in any time
6. critical
   - exit in any time