#include "hoonStat.hpp"

hoonStat::hoonStat()
{
    //Set spdlog
    try 
    {
        auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        console_sink->set_level(_CONSOLE_LOG_LEVEL);

        auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(_LOG_PATH, true);
        file_sink->set_level(_FILE_LOG_LEVEL);

        spdlog::sinks_init_list sinks = {console_sink, file_sink};
        this->hoonStatLogger = std::make_shared<spdlog::logger>("mainLogger", sinks);
    }
    catch (const spdlog::spdlog_ex &ex)
    {
        std::cout << "Log init failed: " << ex.what() << std::endl;
    }

    this->hoonStatLogger->trace(FUNCTION_OUT);
}